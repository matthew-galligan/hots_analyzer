# A HOTS Statistics Analyzer / Advisor

This app provides statistic analysis for Heros of the Storm players and provides advice where a player has statistically higher or lower than average performance with various characters and build orders.

## Images included are the property of Blizzard and protected under copyright.
