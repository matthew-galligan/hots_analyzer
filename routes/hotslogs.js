var request = require('request');
var jsdom = require("jsdom");
var xregexp = require('xregexp');
var request = require('request');

hots = {};
hots.namePattern = new xregexp("^[\\p{L}#\\p{N}]+$");
hots.idPattern = new RegExp("^[\\d]+$");
hots.mmrPattern = new RegExp("\(Current MMR: ([\\d]+)\)")
var aDayInMilliseconds = (24 * 60 * 60 * 1000);

function GetRows(window) {
	var rows = [];
	window.$(".tab-pane.active").find("tr.rgRow, tr.rgAltRow").each(function() {
		var heroName = window.$(this).find("td:eq(2)").text();
		var heroWin = window.$(this).find("td:eq(6)").text().replace("%", "").trim();
		var heroGames = window.$(this).find("td:eq(4)").text();
		console.log(heroName + " - " + heroWin);
		rows.push({
			name: heroName,
			win: heroWin,
			games: heroGames
		});
	});
	return rows;
}

//get hero and win % from main default page
function GetHeroStatRows(window) {
	var rows = [];
	window.$("table:eq(1)").find("tr.rgRow, tr.rgAltRow").each(function() {
		var heroName = window.$(this).find("td:eq(1)").text();
		var heroWin = window.$(this).find("td:eq(5)").text().replace("%", "").trim();

		if (heroName == "Cho'gall") {
			rows.push({
				name: "Cho",
				win: heroWin
			});
			rows.push({
				name: "Gall",
				win: heroWin
			});

		}
		//console.log(heroName + " - " + heroWin);
		rows.push({
			name: heroName,
			win: heroWin
		});
	});
	return rows;
}

//callback returns a data array and an error message.
//if an error has occured, the array is null
hots.getPlayer = function(name, playerid, existingPlayer, charindex, callback) {

	//CALLBACK HELL, LETS GO!!!11!

	//if the player exists and the record is newer than 24 hours, use that.
	if (existingPlayer && !IsADayOld(existingPlayer.timestamp)) {

		//if the existing player is a singleton, just give the player, otherwise go to the selector		
		console.log(new Date(existingPlayer.timestamp));
		console.log("Getting from cache");
		callback(existingPlayer, "OK");
	} else {



		//otherwise, grab a new one from hotslogs, and save to our db.

		var url = "";
		if (name) {
			var isBasicName = hots.namePattern.test(name);
			console.log("Is Basic name: " + name + " ?:" + isBasicName);
			if (isBasicName) {
				console.log(name);
				if (name.includes("#")) {
					console.log("Name includes #");
					GetAPI(1, name, charindex, callback);

					//go get the guy at this url, then get a new URL
				} else {
					var criteria = {
						name: name,
						//timestamp:  { $gte:  new Date(new Date().getTime() - aDayInMilliseconds)}
					};
					charindex.collection('lists').findOne(criteria, function(err, result) {
						if (err) {
							console.log("Error:" + err);
						}
						if (result && !IsADayOld(result.timestamp)) {
							console.log("List exists for player " + name);
							callback(result.players, "PICK");
						} else {
							url = 'https://www.hotslogs.com/PlayerSearch?Name=' + encodeURIComponent(name);
							ReadURL(url, name, charindex, callback);
						}

					});

					//if I am searching for a player, first check my lists to ensure it is not there, and it is not too old.
					//If so, return that instead using the callback.

					//If I can't find an appropriate list, THEN ReadURL


				}
				console.log("URL:" + url);
			}
		} else if (playerid) {

			var isBasicID = hots.idPattern.test(playerid);
			if (isBasicID) {
				url = 'https://www.hotslogs.com/Player/Profile?PlayerID=' + encodeURIComponent(playerid);
				ReadURL(url, name, charindex, callback);
			}
		} else {
			console.log("had neither name or id");
			ReadURL(url, name, charindex, callback);
		}
		//if we are a name, set the url just so

		//if we are an id, set the url for ID



	}
};

function GetAPI(region, name, charindex, callback) {
	restUrl = "https://www.hotslogs.com/API/Players/" + region + "/" + name.replace("#", "_");
	console.log(restUrl);
	request(restUrl, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			if (body == "null") {
				if (region < 4) {
					GetAPI(region + 1, name, charindex, callback);
				} else {
					callback(null, "ERROR");
				}
			} else {
				var info = JSON.parse(body);
				if (info && info.PlayerID) {
					console.log("Getting new URL");
					url = 'https://www.hotslogs.com/Player/Profile?PlayerID=' + encodeURIComponent(info.PlayerID);
					console.log(url);
					ReadURL(url, name, charindex, callback);
				}

			}

		}

	});
}

//post a search, and run the callback against error, response, body. Post twice if I redirect.
function PostSearch(url,callback) {
    console.log('Posting to' + url);
    request.post(
        {
            url: url,
            followAllRedirects: true,
            form: {
                'ctl00$MainContent$DropDownProfileTimeSpan': '-60',
                'ctl00$MainContent%24DropDownGameMode': '-1'
            }

        }, function (error, response, body) {


            var finalPath = response.request.uri.href;

            if (finalPath.includes('PlayerID')) {
                console.log('Posting to' + finalPath);
                request.post(
                    {
                        url: finalPath,
                        followAllRedirects: true,
                        form: {
                            'ctl00$MainContent$DropDownProfileTimeSpan': '-60',
                            'ctl00$MainContent%24DropDownGameMode': '-1'
                        }

                    },
                    function (error, response, body) {
                        callback(error, response, body);
                    });

                //post again

            }
            else {
                callback(error, response, body);
            }

        });
}



//Read a URL (Player profile or player select) and call the callback with the player, or a null/error
function ReadURL(url, name, charindex, callback) {
	if (url != "") {
		//make a request, and await the returns

		//setup body request
            PostSearch(url,function(error,response,body)
        {

			jsdom.env(
					body,  ["http://code.jquery.com/jquery.js"],
					function (err, window) {
						var title = window.$(this).find('#h1Title').text();

						console.log("Title:" + title);
						if (title.includes('Player Search')) {
							var rows = [];
							window.$("table:eq(1)").find("tr.rgRow, tr.rgAltRow").each(function () {

								var region = window.$(this).find("td:eq(1)").text();
								var player = window.$(this).find("td:eq(2)").text();
								var rating = window.$(this).find("td:eq(3)").text() || "-";
								var games = window.$(this).find("td:eq(4)").text();

								var link = "";
								link = window.$(this).find("td:eq(3)").find("a").attr("href").trim();
								var n = link.lastIndexOf('=');
								var id = link.substring(n + 1);
								//console.log(heroName + " - " + heroWin);
								rows.push({
									region: region,
									name: name,
									rating: rating,
									games: games,
									id: id
								});
							});

							if (rows.length < 1) {
								callback(null, "ERROR");
							} else {
								//if i get playersearch, do the search
								//here we redirect the player to a chooser, which reroutes the player all the way back through to lookup via ID
								//get the list of folks, and redirect to choose page.
								var playerList = {
									name: name,
									players: rows,
									timestamp: new Date().getTime(),
								};
								//remove any existing list with this player
								charindex.collection('lists').remove({
									name: name
								}, null, function (err, collection) {
									charindex.collection('lists').insert(playerList);
								});
								//and write a new one

								callback(rows, "PICK");
							}
						} else {

							var singleton = false;
							//if I hit the playersearch, and got only one profile, I'm a singleton (no other names with my name)
							if (url.includes("PlayerSearch")) {
								singleton = true;
							}
							//otherwise go to player profile
							var title = window.$(this).find('#h1Title').text();
							var profileName = window.$("h1").text();
							var heroMMR = null;
							var quickMMR = null;
							var teamMMR = null;
							window.$(".tableGeneralInformation").find("tr").each(function () {

								var text = window.$(this).find("th").text();
								var value = window.$(this).find("td").text();
								if (text.includes("Quick")) {
									if (hots.mmrPattern.test(value)) {
										quickMMR = hots.mmrPattern.exec(value)[2];
									}
								}
								else if (text.includes("Hero")) {
									if (hots.mmrPattern.test(value)) {
										heroMMR = hots.mmrPattern.exec(value)[2];
									}
								}
								else if (text.includes("Team")) {
									if (hots.mmrPattern.test(value)) {
										teamMMR = hots.mmrPattern.exec(value)[2];
									}
								}
							});
							console.log("Profile name: " + profileName);
							var profileIndex = profileName.lastIndexOf(":");
							var playerName = profileName.substring(profileIndex + 1).trim();
							var lowerName = playerName.toLowerCase();
							//if I can get the ID...
							var n = title.lastIndexOf('=');
							var id = title.substring(n + 1);

							var rows = GetRows(window);
							var player = {
								name: playerName,
								lowerName: lowerName,
								playerid: id,
								teamMMR: teamMMR,
								quickMMR: quickMMR,
								heroMMR: heroMMR,
								timestamp: new Date().getTime(),
								singleton: singleton,
								stats: rows
							};
							console.log("Storing to cache: " + lowerName);
							charindex.collection('players').remove({
								playerid: id
							});
							console.log("Updating Cache");
							charindex.collection('players').insert(player);
							console.log("Returning newly cached.");
							callback(player, "OK");
						}
					}
			);
		});
	} else {
		//return no data
		callback(null, "Invalid name entered.");
	}
}



//get a list of hero names and win %
//check herostats index first, and only fetch if over a day old
hots.getHeroStats = function(charindex, callback) {
	var yesterday = new Date(new Date().getTime() - aDayInMilliseconds);

	charindex.collection('herostats').findOne({}, function(err, result) {
		var existingPlayer = null;
		if (err) {
			console.log(err);
		}
		//if i have a result, it has a time flag, and it is newer than yesterday.
		if (result && result.time && !IsADayOld(result.time)) {
			console.log("Hero Stats exist");
			if (!result.rows) {
				result.rows = [];
			}
			callback(result.rows, "OK");
		} else {
			//do the same thingm. Get all hero names unless the record is old, then get from hotslogs
			jsdom.env(
				'https://www.hotslogs.com/Default', ["http://code.jquery.com/jquery.js"],
				function(err, window) {
					var rows = GetHeroStatRows(window);
					console.log("Getting new stats");
					//remove the index, and insert the new one
					charindex.collection('herostats').remove({});
					charindex.collection('herostats').insert({
						rows: rows,
						time: new Date().getTime()
					});
					callback(rows, "OK");
				});
		}
	});
};
//Returns true if a time is older than 24 hours.
function IsADayOld(time) {
	var yesterday = new Date(new Date().getTime() - aDayInMilliseconds);
	return (time < yesterday);
}

module.exports = hots;