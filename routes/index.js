var express = require('express');
var charindex = require('./charindex');
var heroList = require('./heros');
var hotsLogs = require('./hotslogs');
var router = express.Router();
/* GET home page. */
router.get('/', function(req, res, next) {
	var heros = [];
	console.log(req.query);
	var returnNone = true;
	if (req.query.player || req.query.playerid) {



		heros = heroList.generate();
		//default criteria shouldnt happen but dont want to return everyone through some weirdness		
		
		if (req.query.player) {
			var name = req.query.player.toLowerCase();
			console.log("lowerName:" + name);
			criteria = {
				lowerName: name,
				singleton:true
			};
		} else {
			var playerid = req.query.playerid;
			criteria = {
				playerid: playerid
			};
		}

		if (name || playerid) {			
			returnNone = false;
			console.log('trying to fetch player.');
			console.log("Criteria:" + criteria.name);

			charindex.collection('players').findOne(criteria, function(err, result) {
				var existingPlayer = null;
				if (err) {
					console.log(err);
				}
				if (result) {
					console.log("Player exists");
					existingPlayer = result;
				}
				//get the player from hotslogs
				hotsLogs.getPlayer(name,playerid, existingPlayer, charindex, function(data, error) {
					console.log("Data: " + data);
					console.log("Error:" + error);
					//if i got a player pick message, do so here, and take the player to the right place, otherwise continue
					if (error == "PICK") {
						res.render('playersearch', {
							name: name,
							players: data.sort((a, b) => b.games - a.games)
						});
					}
					else {
						var hideall = false;
						var notfound = false;
						//then get main hero stats
						hotsLogs.getHeroStats(charindex, (heroData, message) => {
							//add to the heros, you jabroni.
							for (hero of heros) {
								hero.hide = true;
								//if we got nothing back, don't bother.
								if (data != null && data.stats != null) {
									for (playerHero of data.stats) {
										if (hero.logs_name == playerHero.name) {
											//console.log(hero);
											//console.log("Not hiding" + hero.logs_name);
											hero.win = playerHero.win;
											if(hero.win == null || hero.win == "")
											{
												hero.hide = true;
											}
											else
											{
												hero.hide = false;
											}
											hero.games = playerHero.games;
										}

									}
								}
								if (heroData != null) {
									for (logsHero of heroData) {
										if (hero.logs_name == logsHero.name) {
											//console.log("Matched ", logsHero.name);
											//console.log(logsHero);
											//console.log(hero);
											hero.totalWin = logsHero.win;
											hero.delta = Math.round(hero.win - hero.totalWin);
											hero.better_worse = ((hero.delta < 0) ? "worse" : "better");
											if (hero.delta >= 0) {
												hero.relativePerformance = "good";
											} else {
												hero.relativePerformance = "bad";
											}

											if (hero.delta >= 10) {
												hero.advice = "You excel with";
											} else if (hero.delta > 5) {
												hero.advice = "You are above average with";
											} else if (hero.delta > -5) {
												hero.advice = "You produce average results with";
											} else {
												hero.advice = "You should probably avoid";
											}


										}
									}
								}
							}
							if(!data)
							{
								data = {name: ""};
								hideall = true;
								notfound = true;

							}
							if(!heros)
							{
								heros = [];
								hideall = true;
								notfound = true;
							}
							res.render('index', {
								name: data.name,
								heros: heros.sort((a, b) => b.delta - a.delta),
								hideall: hideall,
								notfound:notfound
							});
						});
					}
				});


				console.log("result:" + result);
			});
		}
	}
	if (returnNone) {		
		console.log("returned none");
		res.render('index', {
			title: 'Hero Analyzer',
			heros: heros,
			hideall: true,
			notfound: false
		});
	}
});

//see if I need to get hero stats, and if so fetch and save to our DB.
function GetHeroStats() {
	hotsLogs.getHeroStats(function(data, message) {
		for (row of data) {
			console.log(row);
		}
		return data;
	});
}
module.exports = router;